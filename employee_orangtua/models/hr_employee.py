from odoo import models, fields

class HrEmployee(models.Model):

	_inherit = 'hr.employee'

	orangtua = fields.Char(string='Nama Orang Tua')
	nenek = fields.Char(string='Nama Nenek')
	kakek = fields.Char(string='Nama Kakek')

	def isi_pin(self):
		self.pin = self.identification_id
		return self.pin